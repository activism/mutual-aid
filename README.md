# Mutual Aid Networks, aka Mutual Aid Pods (_mutual-aid_)

![mutual-aid-pod](./docs/img/mutual-aid-pod-united.png)

> Mutual Aid Networks are autonomous, self-help organizations controlled by their members. If they enter into agreements with other organizations, including governments, or raise capital from external sources, they do so on terms that ensure democratic control by their members and maintain their cooperative autonomy." [^1]

## What is Mutual Aid?

Mutual Aid involves:

- ✅ Getting people together in your community to provide material support to each other

- ✅ Building relationships with your neighbors based on trust and common interest

- ✅ Making decisions by consensus rather than relying on authority or hierarchy

- ✅ Sharing things rather than hoarding things

- ✅ Treating no one as disposable

- ✅ Providing all kinds of support, ranging from food prep to childcare to translation to emotional support, and recognizing the value of all of them

- ✅ A political education opportunity, where we build the relationships and analysis to understand why we are in the conditions that we’re in

- ✅ Preparation for the next disaster (natural or economic). Next time around we’ll already have relationships with each other and know who is vulnerable and needs support

- ✅ A great jumping off point for other kinds of organizing and movement work

Mutual is *not*...

- 🚫 Quid pro quo transactions

- 🚫 Only for disasters or crises

- 🚫 Charity or a way to “save” people

- 🚫 A reason for a social safety net not to exist

## Building a Neighborhood Pod/Network

"[TOOLKIT: Mutual Aid 101][mutual-aid-101-toolkit]," written by Congresswoman Alexandria Ocasio-Cortez and organizer Mariame Kaba, includes step-by-step instructions for safely building a "neighborhood pod" of people who provide material support to each other in our community.

"[Pod Mapping for Mutual Aid][pod-mapping-for-mutual-aid]," by Rebel Sidney Black, describes Mutual Aid Networks in greater detail.

## The Mission of Mutual Aid Networks

“To create means for individuals to discover and succeed in work they want to do, with the support of their community.”

## Core Principles

 1. **Redesigning Work.**<br>
    Mutual Aid Networks recognize that every individual possesses passions and skills which he or she can contribute to their communities and the larger Mutual Aid Network, and will work to find the highest uses to which those qualities and abilities can be applied.

 1. **Reciprocity.**<br>
    Helping works better as a two-way street. “How can I help you?” becomes “How can we help each other to build the world in which we both live?”

 1. **Respect.**<br>
    Every human being matters. Respect is a foundation for freedom of speech and freedom of religion, and supplies the heart and soul of democracy.

 1. **Voluntary and Open Membership.**<br>
    Mutual Aid Networks are voluntary organizations, open to all persons able to use their services and willing to accept the responsibilities of membership, without gender, social, sexual, racial, political or religious discrimination.

 1. **Democratic Member Control.**<br>
    Mutual Aid Networks are democratic organizations with transparent governance structures controlled by their members, who actively participate in setting their policies and making decisions. Persons serving as elected representatives are accountable to the membership.

 1. **Member Economic Participation.**<br>
    Members contribute equitably to, and democratically control, the capital of their Mutual Aid Networks. At least part of that capital is usually the common property of the Mutual Aid Network. Members usually receive limited compensation, if any, on capital subscribed as a condition of membership.
    
    Members allocate surpluses for any or all of the following purposes: 

    - Developing their Mutual Aid Network co-operative, possibly by setting up reserves, part of which at least would be indivisible;
 
    - Benefiting members in proportion to their contributions within their co-operative; and
 
    - Supporting other activities approved by the membership.

 1. **Autonomy and Independence.**<br>
    Mutual Aid Networks are autonomous, self-help organizations controlled by their members. If they enter into agreements with other organizations including governments, or raise capital from external sources, they do so on terms that ensure democratic control by their members and maintain their cooperative autonomy.

 1. **Education, Training, and Information.**<br>
    Mutual Aid Networks provide education and training for their members, elected representatives, managers and workers so they can contribute effectively to the development of their MAN. The main Mutual Aid Network will inform the general public about the nature and benefits of cooperation.

 1. **Mutual Aid within Mutual Aid Networks.**<br>
    Mutual Aid Networks serve their members most effectively and strengthen the co-operative movement by partnering with other Mutual Aid Networks and  organizations that have aligning principles.

 1. **Tracking and Performance Measures.**<br>
    In order to track their progress in meeting the goals of the Mutual Aid Network Core Principles, Mutual Aid Networks will strive to measure and map their socioeconomic resources and needs, internalizing their “external” costs and benefits to the greatest extent possible.

 1. **Tools and Processes.**<br>
    Mutual Aid Networks will work to utilize expanded definitions and metrics of capital, wealth, debt, growth, and profit to enable the full range of non-monetary elements of their economies to be understood and characterized using the best available tools, technologies, and techniques.

 1. **Building Community Sustainability.**<br>
    Mutual Aid Networks will help build local, bioregional, and global economic systems based on material steady-state operating principles that recognize biophysical limits to growth, with the goal of achieving long-term sustainability – – not just seven, but seventy generations into the future.

## Detroit-based COVID-19 Mutual Aid

 A comprehensive list of [active Mutual Aid Networks in Detroit](https://tinyurl.com/ydbrxgls) dedicated to:

 - Artist support
 - Emotional/spiritual support
 - Entertainment
 - Financial Solidarity
 - Food
 - Health Care
 - Housing
 - Misc.
 - Pet/Child Care
 - Storage
 - Transport
 - Utilities

## References

[^1]: What is a Mutual Aid Network? Anon. (2021). Retrieved 18 February 2021, from https://www.nhclv.org/wp-content/uploads/2015/11/What-is-a-mutual-aid-network-what-is-LVMAN.pdf

[mutual-aid-101-toolkit]: https://tinyurl.com/vsx9hkl
[pod-mapping-for-mutual-aid]: https://tinyurl.com/y6tgbehj
